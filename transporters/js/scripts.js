window.onload = function () {

  // the appearance of the shadow of the menu when scrolling
  $(window).scroll(function () {
    if ($(window).scrollTop() > 0) {
      $('.menu').addClass('shadow-scroll');
    } else {
      $('.menu').removeClass('shadow-scroll');
    }
  });

  // open/close user-block
  $('.menu-toggle').on('click', function () {
    $('.user-block').addClass('open');
    $('.profile').addClass('close');
  });
  $('.profile-close').on('click', function () {
    $('.user-block').removeClass('open');
    // $('.user-block').addClass('close');
    $('.profile').removeClass('close');
  });

  // open/close user-menu
  $('.user-toggle').on('click', function () {
    $('.user-menu').slideToggle(1000, function () {
      $('.user-menu').stop(true);
    });
    $('.fa-angle-down').toggleClass('flip');
  });
  // pop-up
  $('.form-success').fancybox();
  //  send form
  $('#form').ajaxForm();
  // form validate
  $("#form").validate({
    submitHandler: function (form) {
      $("#form").ajaxSubmit(function(){
        console.log("Получилось!");
      });
      $(".form-success").trigger('click');
      $('.check-input').removeClass('error-border');
    },
    invalidHandler: function () {
      // $('.check-input').addClass('error-border');
    },
    rules: {
      email: {
        required: true,
        email: true
      },
      phone: {
        required: true,
        digits: true
      },
      password: {
        required: true,
        minlength: 4
      },
    },
    errorElement: "span",
    focusCleanup: true,
    messages: {
      email: {
        required: "We need your email address to contact you",
        email: "Your email address must be in the format of name@domain.com"
      },
      phone: "Use digits to input phone number",
      password: "Password must contain at least 4 characters.",
    },
    errorClass: "error",
  });


}
