jQuery(document).ready(function() {
  $('#lightSlider').lightSlider({
    item: 1,
    autoWidth: false,
    slideMove: 1, // slidemove will be 1 if loop is true
    slideMargin: 100,

    addClass: '',
    mode: "slide",
    useCSS: true,
    cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
    easing: 'linear', //'for jquery animation',////

    speed: 400, //ms'
    auto: false,
    loop: true,
    slideEndAnimation: true,
    pause: 2000,

    keyPress: false,
    controls: true, // If false, prev/next buttons will not be displayed
    prevHtml: '', // custom html for prev control
    nextHtml: '', // custom html for next control

    rtl: false,
    adaptiveHeight: true,

    vertical: false,
    verticalHeight: 500,
    // vThumbWidth: 100,

    // thumbItem: 10, // number of gallery thumbnail to show at a time
    pager: true,
    gallery: false, // Enable thumbnails gallery
    galleryMargin: 5,
    thumbMargin: 5,
    currentPagerPosition: 'middle',

    enableTouch: true,
    enableDrag: true,
    freeMove: true,
    swipeThreshold: 40,

    responsive: []
  });
});