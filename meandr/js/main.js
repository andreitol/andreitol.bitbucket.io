window.onload = function() {

  // Smooth scroll to the form
  const btn = document.querySelector('[href="#form"]');
  btn.addEventListener(
    'click',
    function(e) {
      form.scrollIntoView({
        behavior: 'smooth',
        block: 'start',
      });
    },
    true
  );

  // Form validation
  const form = document.getElementById('form');
  let inputs = document.querySelectorAll('.check-input');
  let modal = document.getElementById('modal');
  let overlay = document.getElementById('overlay');
  let email = document.getElementById('email');
  let tel = document.getElementById('tel');

  // remove class "error" as type
  for (let i = 0; i < inputs.length; i++) {
    inputs[i].oninput = function() {
      inputs[i].classList.remove('error');
    };
  }

  form.onsubmit = function(e) {
    let error = false;

    for (let i = 0; i < inputs.length; i++) {
      if (inputs[i].value === '') {
        inputs[i].classList.add('error');

        error = true;
      } else {
        inputs[i].classList.remove('error');
        modal.classList.add('modal-show');
        overlay.classList.add('overlay-show');
        e.preventDefault(); // my be comment
      }
    }
    if (!ValidMail()) {
      email.classList.add('error');
      error = true;
    } else {
      email.classList.remove('error');
    }
    if (!ValidPhone()) {
      tel.classList.add('error');
      error = true;
    } else {
      tel.classList.remove('error');
    }

    if (error) {
      e.preventDefault();
      modal.classList.remove('modal-show');
      overlay.classList.remove('overlay-show');
    }
  };

  // Phone number validation
  function ValidPhone() {
    let re = /^((\+?7|8)[ \-] ?)?((\(\d{3}\))|(\d{3}))?([\- ])?(\d{3}[\- ]?\d{2}[\- ]?\d{2})$/;
    let inputTel = tel.value;
    // console.log("tel:",inputTel)
    let valid = re.test(inputTel);
    console.log('telValid:', valid);
    return valid; // true or false
  } // console.log("phone:", ValidPhone())

  // email validation
  function ValidMail() {
    let re = /^[\w]{1}[\w-\.]*@[\w-]+\.[a-z]{2,5}$/i;
    let inputMail = email.value;
    let valid = re.test(inputMail);
    console.log('mailValid:', valid);
    return valid; // true or false
  }
  // console.log("mail:", ValidMail())

  overlay.addEventListener('click', function () {
    overlay.classList.remove('overlay-show');
    modal.classList.remove('modal-show')
  })
};
