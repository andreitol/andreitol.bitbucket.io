$(function () {

	// Slow scroll

	// $('a[href*=#]').bind("click", function (e) {
	// 	var anchor = $(this);
	// 	$('html, body').stop().animate({
	// 		scrollTop: $(anchor.attr('href')).offset().top
	// 	}, 1000);
	// 	// e.preventDefault();
	// 	return false;
	// });



	// Initialize Smoth Scroll
	$('a[href*="#"]').smoothscroll();


	// Initialize slick slider

	$('.slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		dots: true,
		dotsClass: 'slick-dots',
		adaptiveHeight: true,
		lazyLoad: 'progressive',
		autoplay: true,
		autoplaySpeed: 3000,
	});

	$('.slider-3').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		dots: true,
		dotsClass: 'slick-dots',
		adaptiveHeight: true,
		lazyLoad: 'progressive',
		// autoplay: true,
		// autoplaySpeed: 2000,
	});

	// form send
	$('form').submit(function (e) {
		let data = $(this).serialize();
		$.ajax({
			url: 'php/send.php',
			method: 'POST',
			data: data,
			success: function () {},
			error: function () {}
		}).done(function () {
			$(this).find('[class="form-control"]').val();
			$('form').trigger('reset');
		})
		e.preventDefault();
	});

	// form validation
	// $('form').parsley();
	$.validate({
		modules: 'html5'
	});

});

