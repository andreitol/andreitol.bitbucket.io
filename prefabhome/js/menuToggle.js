$(function){
  $('.menuToggle').on('click', function(){ // следим за кликом на кнопке разворачивания меню
     $('.menu').slideToggle(300, function(){ // запускается функция плавной анимации jQuery
      if($(this).css('display') === 'none'){
        $(this).removeAttr('style'); // удаляется стиль display: none
      }
     }); 
  })
};