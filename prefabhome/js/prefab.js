$(document).ready(function () {
    InitSliders();

    $('body').on('click', function (e) {
        if (NotInside($('.top-lang'), e)) {
            CloseLang();
        }
    });
    $('.top-lang').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (!$(this).hasClass('top-lang__open')) {
            CloseLang(false);
        }
        else {
            CloseLang();
        }
    });
    $('.lang').on('click', function (e) {
        if (!$(this).hasClass('lang__active')) {
            e.stopPropagation();
        }
        else {
            if ($('.top-lang').hasClass('top-lang__open')) {
                e.stopPropagation();
                e.preventDefault();
                CloseLang();
            }
        }
    });
    $('.menu-mobile').on('click', function () {
        var nav = $('.nav'),
            overlay = $('.overlay');
        nav.addClass('nav-mobileOpen');
        overlay.addClass('overlay-mobileOpen');
        $('body').css('overflow', 'hidden');
    });
    $('.overlay').on('click', function () {
        var nav = $('.nav'),
            overlay = $('.overlay');
        nav.removeClass('nav-mobileOpen');
        overlay.removeClass('overlay-mobileOpen');
        $('body').css('overflow', '');
    });
    $('.nav__back').on('click', function () {
        var nav = $('.nav'),
            overlay = $('.overlay');
        nav.removeClass('nav-mobileOpen');
        overlay.removeClass('overlay-mobileOpen');
        $('body').css('overflow', '');
    });
});

function InitSliders() {
    $('.productSlider__body').slick({
        dots: false,
        infinite: true,
        //arrows: false,
        slidesToShow: 1,
        prevArrow: $('.productSlider__arrow-left >div'),
        nextArrow: $('.productSlider__arrow-right >div'),
        adaptiveHeight: true
    });
}

function CloseLang(close) {
    if (close === undefined) {
        close = true;
    }
    var menu = $('.top-lang');
    var maxHeight = 0;
    menu.find('.lang').each(function () {
        maxHeight += $(this).outerHeight();
    });
    if (close) {
        if (menu.hasClass('top-lang__open')) {
            menu.removeClass('top-lang__open');
            menu.css('height', maxHeight);
            setTimeout(function () {
                menu.css('height', 63);
            }, 10);
            setTimeout(function () {
                menu.css('height', '');
            }, 200);
        }
    }
    else {
        if (!menu.hasClass('top-lang__open')) {
            menu.addClass('top-lang__open');
            menu.css('height', 63);
            setTimeout(function () {
                menu.css('height', maxHeight);
            }, 10);
            setTimeout(function () {
                menu.css('height', '');
            }, 200);
        }
    }
}


function NotInside(element, event) {
    if (!element.is(event.target) && element.has(event.target).length === 0) {
        return true;
    }
    else {
        return false;
    }
}