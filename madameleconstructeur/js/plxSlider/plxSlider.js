

//Sliders
$('.plxSlider').on('init', function (event, slick) {
    $(slick.$slides[slick.currentSlide]).addClass('slick-active-clone');
});

$('.plxSlider').slick({
    slidesToScroll: 1,
    slidesToShow: 1,
    dots: true,
    appendDots: $('.plxSliderDots__container'),
    arrows: true,
    swipeToSlide: true,
    draggable: true,
    cssEase: 'cubic-bezier(0.4, 0.0, 0.2, 1)',
    prevArrow: $('.plxSliderButtons__arrow-left'),
    nextArrow: $('.plxSliderButtons__arrow-right'),
    touchThreshold: 20,
    autoplay:true,
    autoplaySpeed: 8000,
    speed: 1000,
    responsive: [
        {
            breakpoint: 768,
            settings: {
                speed: 600,
                touchThreshold: 10
            }
        }
    ]
});

var switchingMethod = 'swipe';

$('.plxSlider .slick-dots').on('mousedown', function () {
    switchingMethod = 'dots';
});

$('.plxSliderButtons__arrow-right').on('mousedown', function () {
    switchingMethod = 'arrows-right';
});

$('.plxSliderButtons__arrow-left').on('mousedown', function () {
    switchingMethod = 'arrows-left';
});

$('body').on('mousedown', function (e) {
    if (!IsOrInside($('.plxSliderDots__container .slick-dots'), e)) {
        switchingMethod = 'dots';
    }
    else if (!IsOrInside($('.plxSliderButtons__arrow-right'), e)) {
        switchingMethod = 'arrows-right';
    }
    else if (!IsOrInside($('.plxSliderButtons__arrow-left'), e)) {
        switchingMethod = 'arrows-left';
    }
    else {
        switchingMethod = 'swipe';
    }
});

$('body').on('mousedown', function (e) {
    if (!IsOrInside($('.plxSliderDots__container .slick-dots .slick-active button'), e)) {
        e.stopPropagation();
        e.preventDefault();
    }
});


$('.plxSlider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
    //console.log(currentSlide,nextSlide);
    // console.log(slick.currentDirection);
    // console.log(slick.direction);
    //console.log(slick.changeSlide().data.message);
    console.log(switchingMethod);
    var next = $('[data-slide-id=' + nextSlide + ']'),
        lastSlide = $(this).find('.plxSlide').last(),
        lastReal = lastSlide.prev(),
        lastId = lastReal.data('slide-id');
    var currentDirection;
    if (switchingMethod === 'swipe'){
        //$('.plxSlider').slick('slickSetOption', 'cssEase', 'cubic-bezier(0.0, 0.0, 0.2, 1)');
        if(currentSlide !== nextSlide){
            if(slick.currentDirection === 0){
                currentDirection = 'right';
            }
            else {
                currentDirection = 'left';
            }
        }
        else {
            currentDirection = 'none'
        }
    }
    else if (switchingMethod === 'arrows-left') {
        currentDirection = 'left';
    }
    else if (switchingMethod === 'arrows-right') {
        currentDirection = 'right';
    }
    else {
        if (currentSlide > nextSlide) {
            currentDirection = 'left';
        }
        else if (currentSlide < nextSlide) {
            currentDirection = 'right';
        }
        else {
            currentDirection = 'none';
        }
    }

    //console.log(currentDirection);
    if (currentDirection !== 'none') {
        $('.slick-active-clone').removeClass('slick-active-clone');
        var nextContent = next.find('.plxSlide__frame');
        nextContent.css('transition', 'none');
        next.addClass('plxSlide-' + currentDirection);
        setTimeout(function () {
            nextContent.css('transition', "");
            next.addClass('slick-active-clone');
        }, 10);
    }
});

$('.plxSlider').on('afterChange', function (event, slick, currentSlide, nextSlide) {
    $('.plxSlide-left').removeClass('plxSlide-left');
    $('.plxSlide-right').removeClass('plxSlide-right');
    //$('.plxSlider').slick('slickSetOption', 'cssEase', 'cubic-bezier(0.4, 0.0, 0.2, 1)');
});