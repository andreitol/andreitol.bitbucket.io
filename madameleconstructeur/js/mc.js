$(document).ready(function () {
    Initialize();

    $(window).resize(function () {
        OnInitializeAndResize();
        OnALL();
        SetPaymentPointer();
        SetThumbsPointer();
    });


    $(window).scroll(function () {
        OnScroll();
    });

    $('.process-slider__item').on('click', function () {
        if (!$(this).hasClass('process-slider__item--active')) {
            var targetSlide = $(this).data('linked-slide');
            SwitchProgressSlide(targetSlide);
        }
    });

    $('.process-slider__arrow').on('click', function () {
        if (!$(this).hasClass('process-slider__arrow--disabled')) {
            var nextSlide = $('.process-slider__item--active').data('linked-slide') - 1;
            if ($(this).hasClass('process-slider__arrow-right')) {
                nextSlide += 2;
            }
            SwitchProgressSlide(nextSlide);
        }
    });

    function SwitchProgressSlide(slide) {
        $('.process-slider').slick('slickGoTo', slide);
    }

    $('.process-slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        SwitchProgressSlidePaginator(nextSlide);
    });

    function SwitchProgressSlidePaginator(slide) {
        $('.process-slider__item--active').removeClass('process-slider__item--active');
        $('.process-slider__item[data-linked-slide=' + slide + ']').addClass('process-slider__item--active');
        $('.process-slider__arrow--disabled').removeClass('process-slider__arrow--disabled');
        if (slide === 0) {
            $('.process-slider__arrow-left').addClass('process-slider__arrow--disabled');
        }
        else if (slide === $('.process-slider__item').last().data('linked-slide')) {
            $('.process-slider__arrow-right').addClass('process-slider__arrow--disabled');
        }
    }

    $('.paymentGraph__stageLabel').on('click', function () {
        $('.payment-slider__text-slider').slick('slickGoTo', $(this).data('stage-id'));

        if ($(window).width() <= 740) {
            $('html, body').animate({
                scrollTop: ($(this).parent().offset().top - $('.main-menu').outerHeight())
            }, 400);
        }
    });

    $('.payment-slider__text-slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        SetPaymentPointer(nextSlide);
    });

    $('.project-header__thumbs-slide').on('click', function () {
        var selectedClass = 'project-header__thumbs-slide--selected';
        if (!$(this).hasClass(selectedClass)) {
            $('.' + selectedClass).removeClass(selectedClass);
            $(this).addClass(selectedClass);
            $('.project-header__slider').slick('slickGoTo', $(this).data('slide-id'));
            CheckThumbsArrows($(this).data('slide-id'));
        }
    });

    $('.project-header__thumbs-arrow').on('click', function () {
        if (!$(this).hasClass('project-header__thumbs-arrow--disabled')) {
            var selectedClass = 'project-header__thumbs-slide--selected';
            var currentSelected = $('.' + selectedClass).data('slide-id');
            var slidesCount = $('.project-header__thumbs-slide').last().data('slide-id');
            var newSelected;
            if ($(this).hasClass('project-header__thumbs-arrow--left')) {
                if (currentSelected !== 0) {
                    $('.project-header__slider').slick('slickGoTo', currentSelected - 1);
                    /*$('.project-header__thumbs-slider').slick('slickGoTo', currentSelected - 1);
                     $('.' + selectedClass).removeClass(selectedClass);
                     $('.project-header__thumbs-slide[data-slide-id=' + (currentSelected - 1) + ']').addClass(selectedClass);*/
                    newSelected = currentSelected - 1;
                }
            }
            if ($(this).hasClass('project-header__thumbs-arrow--right')) {
                if (currentSelected < slidesCount) {
                    $('.project-header__slider').slick('slickGoTo', currentSelected + 1);
                    /*if((currentSelected + 1) > (slidesCount - 2)){
                     $('.project-header__thumbs-slider').slick('slickGoTo', (slidesCount - 2));
                     }
                     else {
                     $('.project-header__thumbs-slider').slick('slickGoTo', (currentSelected + 1));
                     }
                     $('.' + selectedClass).removeClass(selectedClass);
                     $('.project-header__thumbs-slide[data-slide-id=' + (currentSelected + 1) + ']').addClass(selectedClass);*/
                }
                newSelected = currentSelected + 1;
            }
            CheckThumbsArrows(newSelected);
        }
    });


    $('.project-header__slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {

        var slidesCount = $('.project-header__thumbs-slide').last().data('slide-id');
        var selectedClass = 'project-header__thumbs-slide--selected';
        var current = $('.project-header__thumbs-slider .slick-current').data('slick-index');
        var currentShift = $('.' + selectedClass).data('slick-index') - current;

        if (nextSlide !== current && nextSlide !== current + 1 && nextSlide !== current + 2) {
            if ((nextSlide - currentShift) > (slidesCount - 2)) {
                $('.project-header__thumbs-slider').slick('slickGoTo', (slidesCount - 2));
            }
            else {
                $('.project-header__thumbs-slider').slick('slickGoTo', (nextSlide - currentShift));
            }
        }

        $('.' + selectedClass).removeClass(selectedClass);
        $('.project-header__thumbs-slide[data-slide-id=' + nextSlide + ']').addClass(selectedClass);

        CheckThumbsArrows(nextSlide);
        if (current === $('.project-header__thumbs-slider .slick-current').data('slick-index')) {
            SetThumbsPointer();
        }
    });

    $('.menu-overlay').on('click', function () {
        $('.mob-nav').removeClass('mob-nav--open');
        $('.menu-overlay').removeClass('menu-overlay--open');
        $('.main-nav').removeClass('main-nav--open');
        LockScroll(false);
    });

    $('.main-nav__item').on('click', function (e) {
        $('.mob-nav').removeClass('mob-nav--open');
        $('.menu-overlay').removeClass('menu-overlay--open');
        $('.main-nav').removeClass('main-nav--open');
        LockScroll(false);
        if ($('.fp-viewing-0').length) {
            e.preventDefault();
        }
    });


    $('.mob-nav').on('click', function () {
        if (!$(this).hasClass('mob-nav--open')) {
            $(this).addClass('mob-nav--open');
            $('.menu-overlay').addClass('menu-overlay--open');
            $('.main-nav').addClass('main-nav--open');
            LockScroll();
        } else {
            $(this).removeClass('mob-nav--open');
            $('.menu-overlay').removeClass('menu-overlay--open');
            $('.main-nav').removeClass('main-nav--open');
            LockScroll(false);
        }
    });

});

var initialized = false;

function Initialize() {
    OnInitializeAndResize();
    OnScrollAndInitialize();
    OnALL();

    InitSliders();
    initialized = true;
}

function OnScroll() {
    OnScrollAndInitialize();
    OnALL();
}

function OnALL() {
}

function OnInitializeAndResize() {
    fulljs();
}

function OnScrollAndInitialize() {
}

$(window).load(function () {
    SetPaymentPointer();
    SetThumbsPointer();
});

function CheckThumbsArrows(newSelected) {
    var slidesCount = $('.project-header__thumbs-slide').last().data('slide-id');
    if (newSelected === 0) {
        $('.project-header__thumbs-arrow--left').addClass('project-header__thumbs-arrow--disabled');
        $('.project-header__thumbs-arrow--right').removeClass('project-header__thumbs-arrow--disabled');
    }
    else if (newSelected === slidesCount) {
        $('.project-header__thumbs-arrow--left').removeClass('project-header__thumbs-arrow--disabled');
        $('.project-header__thumbs-arrow--right').addClass('project-header__thumbs-arrow--disabled');
    }
    else {
        $('.project-header__thumbs-arrow--left').removeClass('project-header__thumbs-arrow--disabled');
        $('.project-header__thumbs-arrow--right').removeClass('project-header__thumbs-arrow--disabled');
    }
}

function SetThumbsPointer() {
    var pointer = $('.project-header__pointer');
    var selected = $('.project-header__thumbs-slide--selected');
    if (pointer.length) {
        var selectedOfset = selected.offset().left + (selected.outerWidth() / 2);
        var pointerOfset = selectedOfset - pointer.parent().offset().left;
        pointer.css('left', pointerOfset);
        if (pointer.hasClass('project-header__pointer--sleeping')) {
            pointer.removeClass('project-header__pointer--sleeping');
        }
    }
}

function SetPaymentPointer(position) {
    var pointer = $('.paymentGraph__pointer');
    if (pointer.length) {
        if (pointer.hasClass('paymentGraph__pointer-hidden')) {
            pointer.removeClass('paymentGraph__pointer-hidden');
            if (!position) {
                CalculatePointer(0, pointer, true);
            }
        }
        else if (typeof(position) === 'undefined') {
            CalculatePointer(pointer.data('current-position'), pointer);
        }
        else {
            CalculatePointer(position, pointer);
            pointer.data('current-position', position);
        }
    }
}

function CalculatePointer(position, pointer) {
    var currentStage = $('.paymentGraph__stageLabel[data-stage-id="' + position + '"]');
    if ($(window).width() > 740) {
        var stageOfset = currentStage.offset().left + (currentStage.outerWidth() / 2);
        var pointerParent = pointer.parent();
        var pointerNewOffset = stageOfset - pointerParent.offset().left;

        pointer.css('left', pointerNewOffset);
        pointer.css('top', '');
        $('.payment-slider__text-slider').css('margin-top', '');
    }
    else {
        var stageOfset = currentStage.offset().top + (currentStage.outerHeight() / 2);
        var pointerParent = pointer.parent();
        var pointerNewOffset = stageOfset - pointerParent.offset().top;

        pointer.css('top', pointerNewOffset);
        pointer.css('left', '');

        var slider = $('.payment-slider__text-slider');
        var sliderParentTop = slider.parent().offset().top;
        var nextSlideHeight = slider.find('.payment-slider__text[data-slick-index=' + position + ']').outerHeight();
        var sliderNewMargin = pointerNewOffset - nextSlideHeight / 2;
        if (pointerNewOffset > nextSlideHeight / 2) {
            slider.css('margin-top', sliderNewMargin);
        }
        else {
            slider.css('margin-top', '');
        }
    }
}

function InitializeFP() {
    if ($('#fullPage').length) {
        $('#fullPage').fullpage({
            easingcss3: "cubic-bezier(0.4, 0.0, 0.2, 1)",
            menu: '#menu',
            anchors: anchors
        });
    }
}

function fulljs() {
    if ($(window).width() <= 740 || $(window).height() <= 720) {
        try {
            $.fn.fullpage.destroy('all');
        }
        catch (err) {

        }
    }
    else {
        if (!$('.fp-viewing-0').length) {
            InitializeFP();
        }
    }
}

function InitSliders() {
    $('.process-slider').slick({
        slidesToScroll: 1,
        slidesToShow: 1,
        dots: false,
        arrows: false,
        swipeToSlide: false,
        draggable: false,
        fade: false,
        cssEase: 'cubic-bezier(0.4, 0.0, 0.2, 1)',
        waitForAnimate: false,
        responsive: [
            {
                breakpoint: 740,
                settings: {
                    adaptiveHeight: true
                }
            }
        ]
    });

    $('.payment-slider__text-slider').slick({
        slidesToScroll: 1,
        slidesToShow: 1,
        dots: false,
        infinite: false,
        prevArrow: $('.payment-slider__arrow--left'),
        nextArrow: $('.payment-slider__arrow--right'),
        swipeToSlide: false,
        draggable: false,
        fade: false,
        swipe: false,
        cssEase: 'cubic-bezier(0.4, 0.0, 0.2, 1)',
        waitForAnimate: false,
        responsive: [
            {
                breakpoint: 740,
                settings: {
                    fade: true,
                    adaptiveHeight: true
                }
            }
        ]
    });

    $('.project-header__slider').slick({
        slidesToScroll: 1,
        slidesToShow: 1,
        dots: false,
        infinite: false,
        arrows: false,
        cssEase: 'cubic-bezier(0.4, 0.0, 0.2, 1)',
        //asNavFor: '.project-header__thumbs-slider',
        waitForAnimate: false,
        responsive: [
            {
                breakpoint: 900,
                settings: {
                    dots: true
                }
            }
        ]
    });

    $('.project-header__thumbs-slider').slick({
        slidesToScroll: 1,
        slidesToShow: 3,
        dots: false,
        infinite: false,
        arrows: false,
        cssEase: 'cubic-bezier(0.4, 0.0, 0.2, 1)',
        waitForAnimate: false
    });
}