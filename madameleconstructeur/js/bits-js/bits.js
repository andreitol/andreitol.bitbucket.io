//Image on load -----

(function ($) {
    $.fn.imgLoad = function (callback) {
        return this.each(function () {
            if (callback) {
                if (this.complete || /*for IE 10-*/ $(this).height() > 0) {
                    callback.apply(this);
                }
                else {
                    $(this).on('load', function () {
                        callback.apply(this);
                    });
                }
            }
        });
    };
})(jQuery);

// -----

window.jQuery(function () {
    /*// detect browser scroll bar width
    var scrollDiv = $('<div class="scrollbar-measure"></div>')
            .appendTo(document.body)[0],
        scrollBarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;*/

    $(document)
        .on('hidden.bs.modal', '.modal', function (evt) {
            LockScroll(false);
        })
        .on('show.bs.modal', '.modal', function () {
            LockScroll();
        });
});

var scrollbarWidth;

function MeasureScrollbar() {
    var scrollDiv = document.createElement("div");
    scrollDiv.className = "scrollbar-measure";
    document.body.appendChild(scrollDiv);

// Get the scrollbar width
    scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
    //console.warn(scrollbarWidth); // Mac:  15

// Delete the DIV
    document.body.removeChild(scrollDiv);
}

function SetScrolMargin() {
    if ($(window).height() < $(document).height()) {
        $(document.body).css('padding-right', scrollbarWidth + 'px');
        $('.fixedElement').css('padding-right', scrollbarWidth + 'px');
    }
}

function RemoveScrollMargin() {
    $(document.body).css('padding-right', '');
    $('.fixedElement').css('padding-right', '');
}

function LockScroll(option) {
    if(option || typeof option === 'undefined'){
        SetScrolMargin();
        $('body').addClass('modal-open');
    }
    else if(!option){
        RemoveScrollMargin();
        $('body').removeClass('modal-open');
    }
}

//double-tap element

var dragging = false;
var scroll;

$(document).ready(function () {

    scroll = $(window).scrollTop();

    $(window).scroll(function () {
        scroll = $(window).scrollTop();
    });

    AdaptAllImages();

    var body = $('body');

    $('.double-tap-element').on('touchend', function (e) {
        if (!dragging) {
            if (!$(this).hasClass('cocked')) {
                e.stopPropagation();
                e.preventDefault();
                $('.cocked').each(function () {
                    $(this).removeClass('cocked');
                });
                $(this).addClass('cocked');
            }
            else {
                $(this).removeClass('cocked');
            }
        }
    });

    body.on("touchstart", function () {
        dragging = false;
    });

    body.on("touchmove", function () {
        dragging = true;
    });

    $(document).on('click', function (e) {
        var object = $('.cocked');

        if (!object.is(e.target) && object.has(e.target).length === 0) {
            object.each(function () {
                $(this).removeClass('cocked');
            });
        }
    });

    //Used slider flag -----

    $('.slick-arrow').on('click', function () {
        var parent = $(this).parents().find('.slick-slider');
        if (parent.length) {
            parent.addClass('used');
        }
    });

});

function AdaptAllImages() {

    $('.adaptive-image img').imgLoad(function () {
        var container = $(this).parents('.adaptive-image');
        ResizeAdaptiveImage(container);
        console.log(container);
    });
}

function ResizeAllFlexImages() {
    $('.flex-image').each(function () {
        ResizeAdaptiveImage($(this));
    });
}

function ResizeAdaptiveImage(container) {
    var imgClass;
    var image = container.find('img');

    var ratio = container.outerWidth() / container.outerHeight();
    imgClass = (image.outerWidth() / image.outerHeight() > ratio) ? 'wide' : 'tall';

    image.removeClass('tall');
    image.removeClass('wide');
    image.addClass(imgClass);
}

//window state

var windowPreviousWidth;
var windowCurrentWidth;
var mobileBreakpoint = 768;
var tabletBreakpoint = 1024;
var desktopBreakpoint = 1280;
var mobileState = 'mobile';
var tabletState = 'tablet';
var desktopState = 'desktop';
var desctopLargeState = 'desktop-lg';

$(document).ready(function () {

    windowPreviousWidth = window.innerWidth;
    windowCurrentWidth = window.innerWidth;

    MeasureScrollbar();

    $(window).resize(function () {
        windowCurrentWidth = window.innerWidth;
        if (windowPreviousWidth >= mobileBreakpoint && windowCurrentWidth < mobileBreakpoint) {
            //console.log("mobile in " + windowPreviousWidth + " " + windowCurrentWidth);
            //$(document).trigger('breakpoint');
            Breakpoint(mobileState);
            windowPreviousWidth = window.innerWidth;
        }
        else if (windowPreviousWidth <= mobileBreakpoint && windowCurrentWidth > mobileBreakpoint && windowCurrentWidth < tabletBreakpoint) {
            //console.log("tablet in " + windowPreviousWidth + " " + windowCurrentWidth);
            //$(document).trigger('breakpoint');
            Breakpoint(tabletState);
            windowPreviousWidth = window.innerWidth;
        }
        else if (windowPreviousWidth >= tabletBreakpoint && windowCurrentWidth < tabletBreakpoint) {
            //console.log("tablet in " + windowPreviousWidth + " " + windowCurrentWidth);
            //$(document).trigger('breakpoint');
            Breakpoint(tabletState);
            windowPreviousWidth = window.innerWidth;
        }
        else if (windowPreviousWidth <= tabletBreakpoint && windowCurrentWidth >= tabletBreakpoint && windowCurrentWidth < desktopBreakpoint) {
            //console.log("desktop in " + windowPreviousWidth + " " + windowCurrentWidth);
            //$(document).trigger('breakpoint');
            Breakpoint(desktopState);
            windowPreviousWidth = window.innerWidth;
        }
        else if (windowPreviousWidth >= desktopBreakpoint && windowCurrentWidth < desktopBreakpoint) {
            //console.log("desktop in " + windowPreviousWidth + " " + windowCurrentWidth);
            //$(document).trigger('breakpoint');
            Breakpoint(desktopState);
            windowPreviousWidth = window.innerWidth;
        }
        else if (windowPreviousWidth < desktopBreakpoint && windowCurrentWidth >= desktopBreakpoint) {
            //console.log("desktop-lg in " + windowPreviousWidth + " " + windowCurrentWidth);
            //$(document).trigger('breakpoint');
            Breakpoint(desctopLargeState);
            windowPreviousWidth = window.innerWidth;
        }
        else {
            windowPreviousWidth = window.innerWidth;
        }
        MeasureScrollbar();
    });
});

function Breakpoint(state) {
    if (true) {
        //console.log(state);
    }
}

function GetCurrentState() {
    var state;
    if (windowCurrentWidth < mobileBreakpoint) {
        state = mobileState;
    }
    else if (windowCurrentWidth >= mobileBreakpoint && windowCurrentWidth < tabletBreakpoint) {
        state = tabletState;
    }
    else if (windowCurrentWidth >= tabletBreakpoint && windowCurrentWidth < desktopBreakpoint) {
        state = desktopState;
    }
    else {
        state = desctopLargeState;
    }
    return state;
}

function Collapse(element, hide) {
    var elementCurrentHeight = element.outerHeight();
    var targetHeight = element.css('height', 'auto').outerHeight();
    element.removeAttr('style');
    element.css({
        overflow: 'hidden'
    });
    if (hide) {
        element.height(targetHeight).animate({height: 0}, 200);
        element.addClass('collapsed');
    }
    else {
        element.height(elementCurrentHeight).animate({height: targetHeight}, 200, function () {
            $(this).removeClass('collapsed');
            $(this).removeAttr('style');
        });
    }
}

function IsOrInside(element, target) {
    if (!element.is(target.target) && element.has(target.target).length === 0) {
        return true;
    }
    else {
        return false;
    }
}

function TriggerChildren(object, delay, initialTimeout) {
    var currentDelay = delay + initialTimeout;
    object.children().each(function () {
        var elemet = $(this);
        setTimeout(function () {
            elemet.addClass('triggered');
        }, currentDelay);
        currentDelay += delay;
    });
}

function UnTriggerChildren(object, delay) {
    setTimeout(function () {
        object.children().each(function () {
            var elemet = $(this);
            elemet.removeClass('triggered');
        });
    }, delay);
}