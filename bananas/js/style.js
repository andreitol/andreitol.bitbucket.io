// Initialize skrollr
var s = skrollr.init();

// Initialize Swiper
var mySwiper = new Swiper('.swiper-container', {
	// Optional parameters
	loop: true,
  autoplay: {
		delay: 5000,
		disableOnInteraction: false,
	},
	// Pagination
	pagination: {
		el: '.swiper-pagination',
		clickable: true
	},
})

// Modal
/* const modal = document.querySelector("#modal");
const modalOverlay = document.querySelector("#modal-overlay");
const closeButton = document.querySelector("#close-button");
const openButton =document.querySelector("#open-button");

closeButton.addEventListener("click", () => {
	modal.classList.toggle("closed");
	modalOverlay.classList.toggle("closed");
});

openButton.addEventListener("click", () => {
	modal.classList.toggle("closed");
	modalOverlay.classList.toggle("closed");
}); */

var modal = document.querySelector("#modal");
// var modalOverlay = document.querySelector("#modal-overlay");
var closeButton = document.querySelector("#close-button");
var openButton = document.querySelector("#open-button");

closeButton.addEventListener("click", function () {
	modal.classList.remove("open")
	modal.classList.add("closed");
	// modalOverlay.classList.toggle("closed");
});

openButton.addEventListener("click", function () {
	modal.classList.remove("closed")
	modal.classList.add("open");
	// modalOverlay.classList.toggle("open");
});