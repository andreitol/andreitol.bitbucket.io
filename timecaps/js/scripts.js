$(function () {
  // Menu

  // $('.user-toggle').on('click', function () {
  //   $('.menu').slideToggle(600, function () {
  //     $('.menu').stop(true);
  //   });
  // });

  $('.menu-toggle').on('click', function () {
    $('.menu').toggleClass('open');
  });

  // Timer
  $('#countdown-clock').countdownClock({
    dateFormat: "YYYY-MM-DD",   // The date input format.
    date: '2046-06-14',            // A date string, required.
    showYears: true,           // Toggles years
    showMonths: false,           // Toggles months
    showDays: true,             // Toggles days
    showHours: true,            // Toggles hours
    showMinutes: true,          // Toggles minutes
    showSeconds: true,          // Toggles seconds
    updateInterval: 1
  });

  // Carousel 1 owl
  $('.slider1').owlCarousel({
    items: 4,
    margin: 40,
    dots: false,
    loop: true,
    autoplay: true,
    responsive: {
      0: {items: 2,
        margin: 20,
      },
      400: {items: 3,
        margin: 30,
      },
      640: {items: 4,
        margin: 40,
      },
      1366:{
        margin: 22,
      }
    }
  });

  // Carousel 2 owl
  $('.slider2').owlCarousel({
    items: 1,
    margin: 10,
    dots: true,
    nav: true,
    navText: ["", ""],
    animateOut: 'fadeOut'
  })
});
