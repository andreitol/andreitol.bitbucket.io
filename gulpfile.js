var gulp = require('gulp'), // Подключаем Gulp
    browserSync   = require('browser-sync').create(),
    sass = require('gulp-sass'), //Подключаем Sass пакет
    cssnano       = require('gulp-cssnano'),// Подключаем пакет для минификации CSS
    pug           = require('gulp-pug'), // Подключаем библиотеку для преобразования pug в   html
  plumber = require('gulp-plumber'), //Подключаем библиотеку для формирования вывода об ошибке, но при этом работа Gulp не прерывается
    sourcemaps    = require('gulp-sourcemaps'); // Подключаем библиотеку для встраивания исходных карт в исходный файл

gulp.task('copy', function () {
  gulp.src('./app/index.html') // Выбираем файл для копирования
      .pipe(gulp.dest('./dist')) // Копируем в папку сборкиindex.html
      .pipe(browserSync.reload({ stream: true }))
});

gulp.task('server', ['sass', 'pug'], function () {
  browserSync.init({
    server: {
      baseDir: "./app/"
    },
    notify: false // Отключаем уведомления
  });
  gulp.watch('./app/**/*.html').on('change', browserSync.reload);
  gulp.watch('./app/**/*.css').on('change', browserSync.reload);
  gulp.watch('./app/**/*.js').on('change', browserSync.reload);
  gulp.watch('./app/**/*.pug', ['pug']);
  gulp.watch('./app/sass/**/*.sass', ['sass']);
});

gulp.task('css-libs', function () {
  return gulp.src('app/css/libs')
    .pipe(cssnano())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('app/css'))
});


gulp.task('sass', function () { // Создаем таск Sass
  return gulp.src('app/sass/**/*.sass') // Берем источник
    .pipe(sourcemaps.init()) // Инициализируем sourcemaps
    .pipe(changed()) // Смотрим, что поменялось
    .pipe(plumber({ // Формируем вывод об ошибке, не прерывая Gulp
      errorHandler: notify.onError(function (err) {
        return {
          title: 'Styles',
          message: err.message
        }
      })
    }))
    .pipe(sass()) // Преобразуем Sass в CSS посредством gulp-sass
    .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true })) // Создаем префиксы
    // .pipe(sourcemaps.write({ includeContent: false }, '.')) // Записываем SouceMaps с расширенными настройками
    // .pipe(sourcemaps.init({ loadMaps: true })) // Инициализируем sourcemap
    .pipe(sourcemaps.write('.')) // Записываем Souce Map "." - ?
    .pipe(gulp.dest('app/css')) // Выгружаем результата в папку app/css
    .pipe(browserSync.reload({ stream: true })) // Обновляем CSS на странице при изменении
});


gulp.task('pug', function () {
  return gulp
    .src("./app/pug/*.pug") // Выбираем файл для компиляции pug
    .pipe(plumber({ // Проверяем на ошибки страницы pug, не останавливая Gulp
        errorHandler: notify.onError(function(err) {
          return { title: "Pug", message: err.message };
        }) }))
    .pipe(pug({ pretty: true })) // Компилируем страницы pug
    .pipe(dest("./app/")) // Выгружаем в папку pug
    .pipe(browserSync.stream()); // Обновляем страницы pug при изменении
});


gulp.task('default', ['server']);